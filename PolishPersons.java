package exercises20171119;

public class PolishPersons {
    private String firstName;
    private String secondName;
    private String pesel;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    PolishPersons(String firstName, String secondName, String pesel){
        setFirstName(firstName);
        setSecondName(secondName);
        setPesel(pesel);
    }

    @Override
    public String toString(){
        return ("Imie: " + getFirstName() + ", Nazwisko: " + getSecondName() + ", PESEL: " + getPesel());
    }
}