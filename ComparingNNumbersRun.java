package exercises20171119;

import java.util.ArrayList;

public class ComparingNNumbersRun {

    public static void main(String[] args) {

        NumberFromUser numberFromUser = new NumberFromUser();
        ArrayList<Integer> integerList = new ArrayList<Integer>();
        Integer howNumbers = 0;

        howNumbers = numberFromUser.numberFromUser("Ile liczb chcesz porównać");

        for (int i = 0 ; i < howNumbers ; i++){
            Integer number = numberFromUser.numberFromUser("Podaj liczbę całkowitą");
            integerList.add(number);
        }

        Integer min = integerList.get(0);
        Integer max = integerList.get(0);
        Integer sum = 0;

        for(Integer i: integerList) {
            if(i < min) min = i;
            if(i > max) max = i;
            sum = sum + i;
        }

        System.out.println("\nmin = " + min);
        System.out.println("max = " + max);
        System.out.println("sum = " + sum);

    }
}