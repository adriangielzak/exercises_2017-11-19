package exercises20171119;

import java.util.ArrayList;
import java.util.Random;

public class DrawingOfTwentyUniqueNumbers {

    public static void main(String[] args) {

        ArrayList<Integer> uniqueNumbers = new ArrayList<Integer>();
        Random random = new Random();

        for ( int i = 0 ; uniqueNumbers.size() < 20 ; i++) {

                //Draws an integer between 0 and ...
                Integer drawn = random.nextInt(100);

                //If list "uniqueNumbers" NOT contains drawn number, add to list
                if ( !uniqueNumbers.contains(drawn) ) {
                    uniqueNumbers.add(drawn);
                }
            }

            for (Integer itemList : uniqueNumbers) {
                System.out.println(itemList);
            }
    }
}